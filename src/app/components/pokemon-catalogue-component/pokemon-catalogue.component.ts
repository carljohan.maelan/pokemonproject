import { Component, Input, OnInit } from '@angular/core';
import { PokemonService} from 'src/app/services/pokemon.service';
import { Pokemon } from 'src/app/models/pokemon.model';


@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit {

  
  @Input() pokemon: Pokemon[] = []
  

  constructor(
   
    
  ) { }
    
  ngOnInit(): void {
  
  }
}
