import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  // get user(): { <-- måste definera userService 
    
  // }

  constructor(
    //private readonly userService: UserService
  ) { }

  public logout(){
    sessionStorage.removeItem('pokemon-trainer')
  }

  ngOnInit(): void {
  }

}
