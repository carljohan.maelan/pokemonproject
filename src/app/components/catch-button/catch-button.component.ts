import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchServiceService } from 'src/app/services/catch-service.service';
import { PokemonItemComponent } from '../pokemon-item/pokemon-item.component';
import { TrainerService } from 'src/app/services/trainer.service';


@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css']
})

export class CatchButtonComponent implements OnInit {

  public isCaught: boolean = false;

  @Input() pokemonId: number = 0;
  @Input() pokemonName: string =""
  @Input() pokemonPicture: string = ""
  
  get loading(): boolean{
    return this.catchService.loading;
  }


  constructor(
    private trainerService: TrainerService,
    private readonly catchService: CatchServiceService
  ) { }

  
  


  ngOnInit(): void {

    this.isCaught = this.trainerService.alreadyCaught(this.pokemonId);
  }

  onCatch(): void{


    this.catchService.catchPokemon(this.pokemonId, this.pokemonName, this.pokemonPicture)
    .subscribe({
      next: (trainer: Trainer) => {
        this.isCaught = this.trainerService.alreadyCaught(this.pokemonId)
      },
      error: (error: HttpErrorResponse)=>{
        console.log("ERROR", error.message);
        
      }
    })
  }

}
