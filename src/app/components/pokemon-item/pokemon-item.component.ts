import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';


@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.css']
})
export class PokemonItemComponent implements OnInit {


  @Input() pokemon?: Pokemon;

  constructor(
    private readonly pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
  

  }
}
