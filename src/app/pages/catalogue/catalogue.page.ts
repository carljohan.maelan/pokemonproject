import { Component, OnInit } from '@angular/core';
import {  Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit {

  
  get loading(): boolean {
    return this.pokemonService.loading;
  }

  get error(): string {
     return this.pokemonService.error
  }

  get pokemon(): Pokemon[]{
    return this.pokemonService.pokemonList
  }

  
  constructor(
    private readonly pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
    this.pokemonService.getPokemon();
  }

}
