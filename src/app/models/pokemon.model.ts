export interface Pokemon {
    name: string;
    id: number;
    picture: string;
}


export interface PokemonList{
    count: number;
    next: string
    previous: string;
    results: Result[];

}

export interface Result {
    name: string;
    url: string;
}