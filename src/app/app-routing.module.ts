import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guard/auth.guard";
import { CataloguePage } from "./pages/catalogue/catalogue.page";
import { LoginPage } from "./pages/login/login.page";

import { TrainerPagePage } from "./pages/trainer-page/trainer-page.page";


const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/login"
    },
    {
        path: "login",
        component: LoginPage
    },
    {
        path: "catalogue",
        component: CataloguePage,
        canActivate: [AuthGuard]
    },
    {
        path: "trainer-page",
        component: TrainerPagePage,
        canActivate: [AuthGuard]
    }

]

//Used to import module
@NgModule({
    imports: [  
        RouterModule.forRoot(routes)
    ], 

    // Used to expose module and its features
    exports: [
        RouterModule
    ]  
})
export class AppRoutingModule{

}