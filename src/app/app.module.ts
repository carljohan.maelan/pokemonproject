import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { TrainerPagePage } from './pages/trainer-page/trainer-page.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PokemonCatalogueComponent } from './components/pokemon-catalogue-component/pokemon-catalogue.component';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { PokemonItemComponent } from './components/pokemon-item/pokemon-item.component';
import { FormsModule } from '@angular/forms';
import { CatchButtonComponent } from './components/catch-button/catch-button.component';
import { TrainerComponent } from './components/trainer/trainer.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    TrainerPagePage,
    LoginFormComponent,
    NavbarComponent,
    PokemonCatalogueComponent,
    CataloguePage,
    PokemonItemComponent,
    CatchButtonComponent,
    TrainerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
