import { TestBed } from '@angular/core/testing';

import { CatchServiceService } from './catch-service.service';

describe('CatchServiceService', () => {
  let service: CatchServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatchServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
