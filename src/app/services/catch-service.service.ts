import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonService } from './pokemon.service';
import { TrainerService } from './trainer.service';

const { apiUsers, apiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class CatchServiceService {

  private _loading: boolean = false;

  get loading(): boolean{
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonService,
    private readonly trainerService: TrainerService,
  ) { }




  public catchPokemon(pokemonId: number, pokemonName: string, pokemonPicture: string): Observable<Trainer>{
   
   
    if (!this.trainerService.trainer) {
      throw new Error("catchPokemon: no user logged in");
    }
    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId, pokemonName, pokemonPicture);

    if (!pokemon) {
      throw new Error("catchPokemon: no pokemon with this id: " + pokemonId)
    }

    if (this.trainerService.alreadyCaught(pokemonId)) {
      this.trainerService.releasePokemon(pokemonId);
    } else{
      this.trainerService.catchPokemon(pokemon)
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    })

    this._loading = true;
 
    return this.http.patch<Trainer>(`${apiUsers}/${trainer.id}`,{
      pokemon: [...trainer.pokemon]
    },{
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer)=>{
        this.trainerService.trainer = updatedTrainer;
      }),
      finalize(()=> {
        this._loading = false;
      })
    )
  }
}
