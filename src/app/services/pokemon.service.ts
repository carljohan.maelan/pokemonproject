import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonList, Result } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.utils'


const { apiPokemon } = environment;

@Injectable({
  providedIn: 'root'
})



export class PokemonService {
 



private _loading: boolean = false;

private _error: string = "";
  
private _pokemonList: Pokemon[] = [];


  get pokemonList(): Pokemon[] {
    return this._pokemonList;
  }

 
  get loading() : boolean{
    return this._loading;
  }

  get error() : string{
    return this._error;
  }
  
  
  constructor(private readonly http: HttpClient ) { }


  
  public getPokemon(){

    let pokemons: Pokemon[] | undefined =
    StorageUtil.storageRead('pokemons');
    // console.log('Pokemons ' + pokemons);
    if (pokemons !== undefined){
      this._pokemonList = pokemons;
      console.log("did not fetch")
    } else {
      this.fetchPokemon(); 
    }
  }

  public fetchPokemon(): void{
    this._loading = true;
    this.http.get<PokemonList>(apiPokemon)
    .pipe(
      finalize(()=> {
        this._loading = false;
      })
    )
    .subscribe((Response: any) => {
    let tempPokemonArray: Pokemon[] = []
    for (let i = 0; i < Response.results.length; i++) { 
    let url = Response.results[i].url;
     this.http.get(url)
    .subscribe((specificResponse: any)=>{
     tempPokemonArray[i] = {id: i + 1, name: Response.results[i].name, picture: specificResponse.sprites.front_default}
      this._pokemonList = tempPokemonArray; 
      StorageUtil.storageSave('pokemons', this._pokemonList) 
      console.log("fetched");     
       } )   
      }   
    })
    error: (error: HttpErrorResponse) =>{
      this._error = error.message;
    }
  }

  public getPokemonById(){
    console.log( "test " + sessionStorage.getItem('pokemons'));
  
    return sessionStorage.getItem('pokemons')
  }

  public pokemonById(id: number, name: string, picture: string): Pokemon | undefined{
  
  
  return this._pokemonList.find(pokemon => pokemon.id == id && pokemon.name == name && pokemon.picture == picture )
     
     
  }
}



 