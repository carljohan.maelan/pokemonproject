import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _user?: Trainer;


  get trainer(): Trainer | undefined {
    return this._user;
  }

  set trainer(user: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, user!);
    this._user = user;

  }
  constructor() {
    this._user = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
   }

   public getTrainer(){
    const currentTrainer = sessionStorage.getItem('pokemon-trainer')
   }

    public alreadyCaught(id: number): boolean{
      if(this._user){
      return Boolean(this._user?.pokemon.find((pokemon: Pokemon)=> pokemon.id === id))
    }
      return false;
    }


    public catchPokemon(pokemon: Pokemon): void{
      if (this._user) {
        this._user.pokemon.push(pokemon);
      }
    }

    public releasePokemon(pokemonId: number): void{
      if (this._user) {
        this._user.pokemon = this._user.pokemon.filter((pokemon: Pokemon) => pokemon.id !== pokemonId)
      }
    }
}
