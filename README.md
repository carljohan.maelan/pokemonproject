# Pokemon Project

Pokémon Project is a Angular application using the PokeAPI to catch pokemons to your own profile.

## Description
This is a simple Pokémon application using the PokeAPI to capture the 151 different Kanto Pokémon. You can login and create your own user, catch Pokémon from the catalogue page, and browse your caught Pokémon in the Profile page.

Gotta Catch em All!

## Installation

```
npm-install
```

```
ng serve
```


## Usage


Hidden keys:
In order to access the API you need a API-key aswell as a API-Url. Create a environments folder in the src folder of the solution and add a environment.ts file to your folder. Add environment files src/environments/environment.prod.ts
src/environments/environment.ts to .gitignore. The API for trainers was cloned from dewalds repository([dewald-els](https://github.com/dewald-els/noroff-assignment-api))
The API for Pokémon is from PokeAPI([PokeAPI](https://pokeapi.co/))

```
apiUsers = <your url>
```
```
apiKey: <your key>
```
```
apiPokemon: "https://pokeapi.co/api/v2/pokemon?limit=151"
```

LoginPage:
At the Loginpage you will be faced with a input field in which you need to either login as an existing trainer with your username, or create a new trainer with a username of your choosing. 

CataloguePage:
At the Cataloguepage you can browse all the 151 Kanto Pokémon and catch them which will store the Pokémon you catch to your trainer!

TrainerPage:
At the Userpage you can view your caught Pokémon and should you get tired of a Pokémon (Magikarp?) you can release it into the wild again!

Logout:
Once you're done using the application you can logout which will return you to the Loginpage.

## Authors
Johan Lantz([johan.lantz.a](https://gitlab.com/johan.lantz.a))
Carl-Johan Maelan([carljohan.maelan](https://gitlab.com/carljohan.maelan))


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
